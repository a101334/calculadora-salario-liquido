const date = new Date();
document.getElementById('title').innerHTML = 'Calculadora de Salário Líquido ' + date.getFullYear(); 

const addDiscount = ()=> {
    const descriptionDiscount = document.getElementById('description-discount').value;
    const valueDiscount = document.getElementById('value-discount').value.replace(',','.');

    if (!valueDiscount || valueDiscount == null || valueDiscount <= 0 || isNaN(valueDiscount)) {
        return;
    }

    let elementDiscounts = document.getElementById('insert-info-discounts');
    const countDiscountsItems = document.querySelectorAll('.discount-item').length;
    const discountElement = `<div id="element-discount-${countDiscountsItems}" class="col-10 dflex-row card-discount-item">
                                <span class="col-6">${descriptionDiscount}</span>
                                <input id="discount-${countDiscountsItems}" type="text" class="col-2 discount-item" value="${valueDiscount}">
                                <div class="remove-item" onclick="removeDiscount(this);" data-id-element-discount="${countDiscountsItems}">X</div>
                            </div>`;
    elementDiscounts.innerHTML += discountElement;
}

const removeDiscount = (objDiscount)=> {
    const idElementDiscount = objDiscount.getAttribute('data-id-element-discount');
    document.getElementById(`element-discount-${idElementDiscount}`).remove();
}

const calcDiscountINSS = (salary)=> {

    const resultINSS = {
        discount: 0,
        ref: 0
    }

    if (salary <= 1659.38) {
        resultINSS.discount = salary*8/100;
        resultINSS.ref = 8;
    } else if(salary <= 2765.66) {
        resultINSS.discount = salary*9/100;
        resultINSS.ref = 9;
    } else if(salary <= 5531.31) {
        resultINSS.discount = salary*11/100;
        resultINSS.ref = 11;
    } else {
        resultINSS.discount = 604.44;
        resultINSS.ref = '- ';
    }

    return resultINSS;
}

const calcDiscountIRRF = (salaryBase, numDependents)=> {
    const salaryBaseIRRF = salaryBase - numDependents*189.59;

    const resultIRRF = {
        discount: 0,
        ref:0
    }

    if(salaryBaseIRRF < 1903.99 ) {
        return resultIRRF;
    }

    if (salaryBaseIRRF <= 2826.65) {
        resultIRRF.discount = salaryBaseIRRF*7.5/100 - 142.80;
        resultIRRF.ref = 7.5;
    } else if(salaryBaseIRRF <= 3751.05) {
        resultIRRF.discount = salaryBaseIRRF*15/100 - 354.80;
        resultIRRF.ref = 15;
    } else if(salaryBaseIRRF <= 4664.68) {
        resultIRRF.discount = salaryBaseIRRF*22.5/100 - 636.13;
        resultIRRF.ref = 22.5;
    } else if(salaryBaseIRRF >= 4664.69) {
        resultIRRF.discount = salaryBaseIRRF*27.5/100 - 869.36;
        resultIRRF.ref = 27.5;
    } 

    return resultIRRF;
}

const calcLiquidSalary = ()=> {
    const salary = document.getElementById('salary').value.replace(',','.');

    if(isNaN(salary)) {
        document.getElementById('salary').value = '';
        return;
    }

    if (!salary || salary == null || salary <= 0) {
        return;
    }

    const nDependents = document.getElementById('dependents').value;

    const resultINSS = calcDiscountINSS(salary);
    const salaryBase = salary - resultINSS.discount;
    const resultIRRF = calcDiscountIRRF(salaryBase, nDependents);

    const cardDiscountItems = document.querySelectorAll('.card-discount-item');

    
    let liquidSalary = salary - resultINSS.discount - resultIRRF.discount;

    cardDiscountItems.forEach(cardDiscountItem => {
        
        liquidSalary -= cardDiscountItem.children[1].value;
    });

    let stringResult = `<h1 class="col-10">Resultado</h1>
                        <table class="col-9 table">
    
                            <tr>
                                <th>
                                    Descrição
                                </th>
                                <th>
                                    Ref.
                                </th>
                                <th>
                                    Valor
                                </th>
                            </tr>`;
    stringResult += createTrResponse('Salário Bruto', '-', parseFloat(salary), 'add-value-table');
    stringResult += createTrResponse('INSS', `${resultINSS.ref}%`, resultINSS.discount, 'discount-table');
    stringResult += createTrResponse(`IRRF ${date.getFullYear()}`, `${resultIRRF.ref}%`, resultIRRF.discount, 'discount-table');                        
    cardDiscountItems.forEach(cardDiscountItem => {
        stringResult += createTrResponse(cardDiscountItem.children[0].innerHTML, '-', cardDiscountItem.children[1].value, 'discount-table');
    });

    stringResult += createTrResponse('Salário Líquido', '-', liquidSalary, 'add-value-table');
    stringResult += '</table>';
    
    document.getElementById('result').innerHTML = stringResult;
}

const createTrResponse = (description, ref, value, classTdValue) => {
    const trString = `<tr>
                        <td>
                            ${description} 
                        </td>
                        <td>
                            ${ref} 
                        </td>
                        <td class="${classTdValue}">
                            ${classTdValue == 'discount-table' ? '-' : ''}${parseFloat(value).toFixed(2)} 
                        </td>
                    </tr>`;
    return trString;
}

const validateMoney = (e) => {
    if(!(e.keyCode >= 48 && e.keyCode <= 57) & e.keyCode != 8 & e.keyCode != 190 & e.keyCode != 188) {
        const removeValue = e.originalTarget.value.slice(0, -1);
        e.originalTarget.value = removeValue;
    }
}